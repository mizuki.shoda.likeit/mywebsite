<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報削除</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/userDelete.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>

	<h1 align="center"><span style="background-color:White">ユーザー削除確認</span></h1>

	<div class="delete-area">
		<div class="card">
			<h5 class="card-header">もう一度確認してください</h5>
			ログインID：${userInfo.loginId}<br>
			ユーザー名：${userInfo.userName}<br>
			を本当に削除してよろしいですか？
		<br>
		<br>

			<div class="row">
				<div class="col-sm-6">
					<a href="UserDataServlet">
						<input type="submit" class="btn btn-primary" value="キャンセル">
					</a>
			</div>

				<div class="col-sm-6">
					<form action="UserDeleteServlet" method="post">
						<input type="submit" class="btn btn-danger" value="ユーザー情報を削除する">
						<input type="hidden" name="id" value="${userInfo.id}">
					</form>
				</div>
			</div>
			<br>
		</div>
		<br>
	</div>
</body>
</html>