<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ポケモン交換詳細</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/pokemonTradeDetail.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面はこちらから</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>
	<h1 align="center"><font color="White">⚡ポケモン交換詳細⚡</font></h1>

		<div class="pokemonTradeDetail-area">
		<div class="card">
			 <h5 class="card-header">ポケモンのユーザー</h5>
				<div class="card-body">
    				<h5 class="card-title">${userInfo.userName}</h5>
    						<p class="card-text">ひとこと：${tradePokemonData.text}</p>
    		</div>
		</div>

		<div class="card">
		 <h5 class="card-header">交換に出すポケモン</h5>

			<div class="card-body">
    		<h5 class="card-title">${tradePokemonData.pokemonName}</h5>
    		<p class="card-text">タイプ：${tradePokemonFirstTypeData.typeName}    ${tradePokemonSecondTypeData.typeName}</p>
    		</div>
		</div>

		<div class="card">
		 <h5 class="card-header">欲しいポケモン</h5>
			<div class="card-body">
    		<h5 class="card-title">${wantPokemonData.pokemonName}</h5>
    		<p class="card-text">タイプ：${wantPokemonFirstTypeData.typeName}    ${wantPokemonSecondTypeData.typeName}</p>
    		</div>

			<c:if test="${userLoginInfo.loginId != userInfo.loginId and favoriteData == null}">
				<div class="center">
					<div class="row">
    					<a href="PokemonTradeBoardServlet?id=${tradeId}" class="col-6">
    						<input type="submit" class="btn btn-success" value="交換を申し込む">
    					</a>
    					<form action="FavoritePokemonConfirmServlet" method="post">
    						<input type="submit" class="btn btn-warning" value="お気に入りに登録する">
    						<input type="hidden" name="tradeId" value="${tradeId}">
    					</form>
    				</div>
    			</div>
    		</c:if>

    		<c:if test="${userLoginInfo.loginId != userInfo.loginId and favoriteData != null}">
				<div class="center">
					<div class="row">
    					<a href="PokemonTradeBoardServlet?id=${tradeId}" class="col-6">
    						<input type="submit" class="btn btn-success" value="交換を申し込む">
    					</a>
    					<a href="FavoritePokemonCancelServlet?id=${tradeId}" class="col-6">
    						<input type="submit" class="btn btn-danger" value="お気に入りを解除する">
    					</a>
    				</div>
    			</div>
    		</c:if>

			<c:if test="${userLoginInfo.loginId == userInfo.loginId}">
				<div class="center">
    					<a href="PokemonTradeBoardServlet?id=${tradeId}" class="col-6">
    						<input type="submit" class="btn btn-success" value="交換の申し込みがあるか確認する">
    					</a>
    				</div>
    		</c:if>
    	</div>
	</div>
</body>
</html>