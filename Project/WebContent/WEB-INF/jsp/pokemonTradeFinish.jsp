<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ポケモン交換完了</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/pokemonTradeFinish.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面はこちらから</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>

	<div class="pokemonTradeFinish-area">
		<div class="card">
			<h5 class="card-header">もう一度確認してください</h5>
			登録しているポケモンが検索結果に表示されなくなります。<br>
			再表示するには、ポケモンの再登録が必要です。<br>
			交換完了してよろしいですか？
		<br>
		<br>

			<div class="row">
				<div class="col-sm-6">
					<form action="PokemonTradeBoardServlet" method="get">
						<input type="submit" class="btn btn-primary" value="キャンセル">
						<input type="hidden" name="id" value="${tradeId}">
					</form>
			</div>
				<form action="PokemonTradeFinishServlet?id=${tradeId}" method="post">
					<div class="col-sm-6">
						<input type="submit" class="btn btn-danger" value="交換完了する">
					</div>
				</form>
			</div>
			<br>
		</div>
		<br>
	</div>
</body>
</html>