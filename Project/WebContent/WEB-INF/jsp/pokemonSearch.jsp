<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ポケモン検索画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/pokemonSearch.css" rel="stylesheet" type="text/css" />
</head>
<body>

	<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面はこちらから</font></b></a>
	</div>
	<br>

	<h1 align="center">
		<font color="White">⚡交換待ちのポケモンを検索してみよう！⚡</font>
	</h1>
	<br>
	<br>

	<div class="pokemonSearch-area">
		<div class="card">
			<h5 class="card-header">「交換に出すポケモン」に登録されているポケモンを探す🔎</h5>

			<div class="card-body">
				<form action="TradePokemonSearchServlet" method="get">
				<div class=row>
					<b class="col-4">ポケモンの名前</b>
					<select class="form-control col-8" name="tradePokemonNameId">
							<option></option>
							<option value="1">フシギダネ</option>
							<option value="2">フシギソウ</option>
							<option value="3">フシギバナ</option>
							<option value="4">ゼニガメ</option>
							<option value="5">カメール</option>
							<option value="6">カメックス</option>
							<option value="7">ヒトカゲ</option>
							<option value="8">リザード</option>
							<option value="9">リザードン</option>
							<option value="10">ピカチュウ</option>
					</select>

					<b class="col-4">ポケモンのタイプ</b>
					<select class="form-control col-8" name="tradePokemonTypeId">
						<option></option>
						<option value="1">ノーマル</option>
						<option value="2">ほのお</option>
						<option value="3">みず</option>
						<option value="4">でんき</option>
						<option value="5">くさ</option>
						<option value="6">こおり</option>
						<option value="7">かくとう</option>
						<option value="8">どく</option>
						<option value="9">じめん</option>
						<option value="10">ひこう</option>
						<option value="11">エスパー</option>
						<option value="12">むし</option>
						<option value="13">いわ</option>
						<option value="14">ゴースト</option>
						<option value="15">ドラゴン</option>
						<option value="16">あく</option>
						<option value="17">はがね</option>
						<option value="18">フェアリー</option>
					</select>
				</div>
				<br>

				<div class="center">
					<input type="submit"class="btn btn-success" value="検索🔎">
				</div>
				</form>
			</div>
		</div>
		<br>
		<br>

		<div class="card">
			<h5 class="card-header">「欲しいポケモン」に登録されているポケモンから探す🔎</h5>

			<div class="card-body">
				<form action="WantPokemonSearchServlet" method="get">
				<div class=row>
					<b class="col-4">ポケモンの名前</b>
					<select class="form-control col-8" name="wantPokemonNameId">
							<option></option>
							<option value="1">フシギダネ</option>
							<option value="2">フシギソウ</option>
							<option value="3">フシギバナ</option>
							<option value="4">ゼニガメ</option>
							<option value="5">カメール</option>
							<option value="6">カメックス</option>
							<option value="7">ヒトカゲ</option>
							<option value="8">リザード</option>
							<option value="9">リザードン</option>
							<option value="10">ピカチュウ</option>
					</select>

					<b class="col-4">ポケモンのタイプ</b>
					<select class="form-control col-8" name="wantPokemonTypeId">
						<option></option>
						<option value="1">ノーマル</option>
						<option value="2">ほのお</option>
						<option value="3">みず</option>
						<option value="4">でんき</option>
						<option value="5">くさ</option>
						<option value="6">こおり</option>
						<option value="7">かくとう</option>
						<option value="8">どく</option>
						<option value="9">じめん</option>
						<option value="10">ひこう</option>
						<option value="11">エスパー</option>
						<option value="12">むし</option>
						<option value="13">いわ</option>
						<option value="14">ゴースト</option>
						<option value="15">ドラゴン</option>
						<option value="16">あく</option>
						<option value="17">はがね</option>
						<option value="18">フェアリー</option>
					</select>
				</div>
				<br>

				<div class="center">
					<a href="pokemonSearchResult.html"><input type="submit" class="btn btn-primary" value="検索🔎"></a>
				</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>