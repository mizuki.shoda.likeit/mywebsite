<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
</head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/userList.css" rel="stylesheet" type="text/css" />

<body>
	<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>

	<h1 align="center"><span style="background-color:White">ユーザー一覧</span></h1>

	<div class="userList-area">
		<div class="card">
			<h5 class="card-header">ユーザーを検索する🔎</h5>

			<div class="card-body">
				<form action="UserListServlet" method="post">
				<div class=row>
					<b class="col-3">ログインID</b>
					<input type="text" name="loginId" class="form-control col-9">
				</div>

				<div class=row>
					<b class="col-3"><span style="background-color:White">ユーザー名</span></b>
					<input type="text" name="userName" class="form-control col-9">
				</div>

				<div class=row>
					<b class="col-3"><span style="background-color:White">生年月日</span></b>
					<input type="date" name="beginningBirthDate" class="form-control col-4">～
					<input type="date" name="endBirthDate" class="form-control col-4">
				</div>
				<br>
				<div class="center">
					<input type="submit" class="btn btn-warning" value="⚡検索⚡">
				</div>
				</form>
			</div>
		</div>
		<br>
		<br>

		<hr>

		<br>
		<br>

		<table border="1">
			<tr>
				<th>ログインID</th>
				<th>ユーザー名</th>
				<th>生年月日</th>
				<th></th>
			</tr>
			<c:forEach var="userData" items="${userList}">
				<tr>
					<td>${userData.loginId}</td>
					<td>${userData.userName}</td>
					<td>${userData.birthDate}</td>
					<td><a href="UserDeleteServlet?id=${userData.id}"><input type="submit" class="btn btn-danger" value="アカウントを削除する"></a></td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>