<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
</head>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/userDataUpdate.css" rel="stylesheet" type="text/css" />
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>

	<h1 align="center"><span style="background-color:White">ユーザー情報更新</span></h1>

	<div class="userUpdate-area">
		<div class="card">
			<h5 class="card-header">入力してください</h5>
				<div class="row">
					<div class="col-sm-6">
						<b>ログインID</b>
					</div>
					<div class="col-sm-6">
						${userLoginInfo.loginId}
					</div>
				</div>
				<br>

				<form action="UserDataUpdateServlet" method="post">
				<div class="row">
					<div class="col-sm-4">
						<b>パスワード</b>
					</div>
					<div class="col-sm-8">
						<input type="password" name="password" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<b>パスワード(確認)</b>
					</div>
					<div class="col-sm-8">
						<input type="password" name="confirmPassword" class="form-control">
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<b>ユーザー名</b>
					</div>
					<div class="col-sm-8">
						<input type="text" name="userName" class="form-control" value="${userInfo.userName}" required>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<b>生年月日</b>
					</div>
					<div class="col-sm-8">
						<input type="date" name="birthDate" class="form-control"  value="${userInfo.birthDate}" required>
					</div>
				</div>

				<br>
					<div class="center">
						<a href="userData.html">
							<input type="submit" class="btn btn-success" value="ユーザー情報を更新する">
						</a>
					</div>
						<input type="hidden" name="id" value="${userInfo.id}">
				</form>
					<br>
		</div>
		<c:if test="${errMsg != null}" >
	    	<div class="alert alert-danger" role="alert">
		  		${errMsg}
			</div>
		</c:if>
	</div>
</body>
</html>