<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/userLogin.css" rel="stylesheet" type="text/css" />

</head>
<body>
<h1 align="center">⚡ポケモン交換会へようこそ⚡</h1>
<br>
<br>
		<div class ="login-area">
			<div class="card">

			<h5 class="card-header">ログインしてください</h5>

				<form action="UserLoginServlet" method="post">
					<div class="card-body">
						<div class="row">
							<b class="col-4">ログインID</b>
		 						<input type="text" name="loginId" class="form-control col-8" required>
							<b class="col-4">パスワード</b>
								<input type="password" name="password" class="form-control col-8" required>
						</div>
						<br>
						<div class="center">
							<input type="submit" class="btn btn-warning" value="⚡ログイン⚡">
						</div>
					</div>
				</form>
			</div>
		</div>
		<br>
		<br>

		<div class ="login-area">
			<div class="card">
			<h5 class="card-header">非会員の方、下記より新規会員登録を行ってください</h5>

				<div class="card-body">
					<div class="center">
						<a href="AddUserServlet"><input type="submit" class="btn btn-danger" value="🔥新規会員登録🔥"></a>
					</div>
				</div>
			</div>
			<br>
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
		  			${errMsg}
				</div>
			</c:if>

			<c:if test="${logoutMsg != null}" >
				<div class="center">
	    			<div class="alert alert-success" role="alert">
		  				${logoutMsg}<br>
		  					<form action="UserLogoutServlet" method="post" >
								<input type="submit" class="btn btn-light" value="閉じる">
		  					</form>
		  			</div>
		  		</div>
			</c:if>
		</div>
</body>
</html>