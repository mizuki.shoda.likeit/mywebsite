<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ポケモン検索結果</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/pokemonSearchResult.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面はこちらから</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>

		<h1 align="center"><font color="White">⚡ポケモン検索結果一覧⚡</font></h1>
		<br>
		<br>
		<div class="pokemonSearchResult-area">

			<div class="row">
			<c:forEach var="tradePokemonData" items="${tradePokemonList}">
				<div class="col-md-4">

					<div class="card mb-4 shadow-sm">
	  				<a href="PokemonTradeDetailFromSearchServlet?id=${tradePokemonData.tradeId}"><img src="picture/${tradePokemonData.image}" class="card-img-top" alt="${tradePokemonData.tradePokemonName}"></a>

	  					<div class="card-body">
	    				<h5 class="card-title">${tradePokemonData.tradePokemonName}</h5>
	    				<p class="card-text">
							タイプ：${tradePokemonData.firstTypeName}    ${tradePokemonData.secondTypeName}<br>
							欲しいポケモン：${tradePokemonData.wantPokemonName}<br>
							ひとこと：${tradePokemonData.text}<br>
	    				</p>
	  					</div>
					</div>
				</div>
			</c:forEach>
			</div>
			<br>
		</div>
</body>
</html>