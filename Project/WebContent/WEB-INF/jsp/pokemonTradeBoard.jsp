<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ポケモン交換掲示板</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/pokemonTradeBoard.css" rel="stylesheet" type="text/css" />
</head>
<body>
<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面はこちらから</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>
	<h1 align="center"><font color="White">⚡ポケモンを交換しよう！⚡</font></h1>

	<div class="pokemonTradeBoard-area">
		<div class="card">
  			<div class="card-header">
    			${userInfo.userName}さんにメッセージを送る
  			</div>

  			<div class="card-body">
  			<form action="PokemonTradeBoardServlet" method="get">
				<div class="form-group">
					<label for="exampleFormControlTextarea1">メッセージを入力してください</label>

    				<textarea class="form-control" name="message" id="exampleFormControlTextarea1" rows="3" required></textarea>

    				<div class="row">
    					<div class="col-sm-2">
    					<input type="hidden" name="id" value="${tradeId}">
    					<input type="submit" class="btn btn-primary" value="送信">
    					</div>
					</div>
				</div>
			</form>
  			</div>
		</div>
		<br>
		<br>
		<c:if test="${userInfo.loginId==userLoginInfo.loginId}">
			<div class="card">
				<h5 class="card-header">交換完了する</h5>
				<div class="card-body">
				<a href="PokemonTradeFinishServlet?id=${tradeId}">
    				<input type="submit" class="btn btn-danger" value="交換完了">
    			</a>
				</div>
			</div>
		</c:if>
		<br>
		<br>
		<div class="card">
  			<div class="card-header">
    			${userInfo.userName}さんとのメッセージ履歴
  			</div>
				<div class="card-body">
					<table border="1">
						<tr>
							<th width="20%">ユーザー名</th>
							<th width="30%">投稿日時</th>
							<th width="50%">メッセージ</th>
						</tr>

						<c:forEach var="messageData" items="${messageDataList}">
						<tr>
							<th>${messageData.userName}</th>
							<th>${messageData.postDate}</th>
							<th>${messageData.message}</th>
						</tr>
						</c:forEach>
					</table>
  				</div>
		</div>
		<br>
		<br>
		<c:if test="${userInfo.loginId!=userLoginInfo.loginId and userLoginInfo.loginId!='admin'}">
			<form action="AddReportServlet" method="post">
				<div class="card">
					<h5 class="card-header">管理者に通報する</h5>
					<div class="card-body">
					通報内容
						<select class="form-control col-8" name="reportId">
							<option value="1">誹謗・中傷の書き込み</option>
							<option value="2">個人情報の掲載</option>
							<option value="3">出会い系・猥褻サイトのリンク</option>
							<option value="4">営利目的となる商用利用</option>
							<option value="5">わいせつ表現</option>
							<option value="6">犯罪・自殺をほのめかす内容</option>
							<option value="7">スパム</option>
							<option value="8">迷惑行為</option>
							<option value="9">その他</option>
						</select>
					</div>
					<div class="card-body">
						<label for="exampleFormControlTextarea1">具体的な理由を記入してください（任意）</label>
    						<textarea class="form-control" name="reportReason" id="exampleFormControlTextarea1" rows="5"></textarea>
    							<input type="submit" class="btn btn-danger" value="通報">
    							<input type="hidden" name="tradeId" value="${tradeId}">
					</div>
				</div>
			</form>
		</c:if>
		<br>
	</div>
	<br>
</body>
</html>