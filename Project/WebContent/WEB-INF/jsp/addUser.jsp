<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link rel="stylesheet"
href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
crossorigin="anonymous">

<link href="css/addUser.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">
		<a href="UserLoginServlet"><b><font color="Blue">ログイン画面に戻る</font></b></a>
	</div>
	<br>
	<h1 align="center"><font color="White"><span style="background-color:Red">🔥ユーザー新規登録🔥</span></font></h1>
	<br>
	<div class="addUser-area">
		<div class="card">
		<h5 class="card-header">ユーザー情報登録</h5>

			<form action="AddUserServlet" method="post">
			 <div class="card-body">
			 	<div class="row">
					<b class="col-4">ログインID</b>
		 				<input type="text" name="loginId" class="form-control col-8" placeholder="半角英数字" required>

					<b class="col-4">パスワード</b>
						<input type="password" name="password" class="form-control col-8" placeholder="半角英数字" required>

					<b class="col-4">パスワード(確認)</b>
						<input type="password" name="confirmPassword" class="form-control col-8" placeholder="確認の為、もう一度入力してください" required>

					<b class="col-4">ユーザー名</b>
						<input type="text" name="userName" class="form-control col-8" required>

					<b class="col-4">生年月日</b>
						<input type="date" name="birthDate" class="form-control col-8" required>
				</div>
			 </div>
			<br>
			 	<div class="center">
					<input type="submit" class="btn btn-danger" value="🔥登録🔥">
				</div>
		 	</form>
		</div>
		<br>
		<c:if test="${errMsg != null}" >
	    	<div class="alert alert-danger" role="alert">
		  		${errMsg}
			</div>
		</c:if>

		<c:if test="${errMsg2 != null}" >
	    	<div class="alert alert-danger" role="alert">
		  		${errMsg2}
			</div>
		</c:if>
	</div>
</body>
</html>