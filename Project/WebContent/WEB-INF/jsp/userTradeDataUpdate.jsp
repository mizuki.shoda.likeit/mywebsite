<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー交換情報</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/userTradeDataUpdate.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>

	<h1 align="center"><span style="background-color:White">交換情報を追加する</span></h1>
	<br>

	<div class="userTradeData-area">
		<div class=card>
		<h5 class="card-header">入力してください</h5>

			<div class="card-body">
				<form action="UserTradeDataUpdateServlet" method="post">
				<div class="row">
					<div class="col-sm-4">
						<b>交換に出すポケモン</b>
					</div>
					<div class="col-sm-8">
						<select class="form-control col-8" name="tradePokemonId">
							<option value="1">フシギダネ</option>
							<option value="2">フシギソウ</option>
							<option value="3">フシギバナ</option>
							<option value="4">ゼニガメ</option>
							<option value="5">カメール</option>
							<option value="6">カメックス</option>
							<option value="7">ヒトカゲ</option>
							<option value="8">リザード</option>
							<option value="9">リザードン</option>
							<option value="10">ピカチュウ</option>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<b>欲しいポケモン</b>
					</div>
					<div class="col-sm-8">
						<select class="form-control col-8" name="wantPokemonId">
							<option value="1">フシギダネ</option>
							<option value="2">フシギソウ</option>
							<option value="3">フシギバナ</option>
							<option value="4">ゼニガメ</option>
							<option value="5">カメール</option>
							<option value="6">カメックス</option>
							<option value="7">ヒトカゲ</option>
							<option value="8">リザード</option>
							<option value="9">リザードン</option>
							<option value="10">ピカチュウ</option>
						</select>
					</div>
				</div>

				<div class="row">
					<div class="col-sm-4">
						<b>ひとこと</b>
					</div>
					<div class="col-sm-8">
						<c:if test="${tradeInfo.text != null}">
							<input type="text" class="form-control" name="text" value="${tradeInfo.text}" required>
						</c:if>
						<c:if test="${tradeInfo.text == null}">
							<input type="text" class="form-control" name="text" placeholder="(例)個体値MAXです" required>
						</c:if>
					</div>
				</div>
				<br>
				<div class="center">
					<a href="userData.html">
						<input type="submit" class="btn btn-success" value="交換情報を追加する">
					</a>
				</div>
				</form>
			</div>
		</div>
		<br>
	</div>
</body>
</html>