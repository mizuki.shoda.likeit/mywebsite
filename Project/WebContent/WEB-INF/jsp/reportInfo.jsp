<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>通報情報一覧</title>
</head>
<body>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/reportInfo.css" rel="stylesheet" type="text/css" />
	<div class="user-area">
		こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
	<br>
	<h1 align="center"><span style="background-color:White">通報情報一覧</span></h1>
	<div class="userList-area">
		<div class="card">
			<h5 class="card-header">通報情報を絞り込む</h5>

			<div class="card-body">
				<form action="ReportInfoServlet" method="post">
					<div class=row>
						<b class="col-5">通報内容</b>
							<select class="form-control col-6" name="concreteReportId">
								<option></option>
								<option value="1">誹謗・中傷の書き込み</option>
								<option value="2">個人情報の掲載</option>
								<option value="3">出会い系・猥褻サイトのリンク</option>
								<option value="4">営利目的となる商用利用</option>
								<option value="5">猥褻表現</option>
								<option value="6">犯罪・自殺をほのめかす内容</option>
								<option value="7">スパム</option>
								<option value="8">迷惑行為</option>
								<option value="9">その他</option>
							</select>
					</div>

					<div class="row">
						<b class="col-5">ログインID(通報した側)</b>
						<input type="text" name="loginId" class="form-control col-6">
					</div>

					<div class="row">
						<b class="col-5">ログインID(通報された側)</b>
							<input type="text" name="reportedLoginId" class="form-control col-6">
					</div>
					<br>
					<div class="center">
						<input type="submit" class="btn btn-warning" value="⚡検索⚡">
					</div>
				</form>
			</div>
		</div>
		<br>
		<hr>
		<br>
		<table border="1">
			<tr>
				<th width="120">ログインID<br>(通報した側)</th>
				<th width="120">通報内容</th>
				<th width="120">通報理由</th>
				<th width="120">ログインID<br>(通報された側)</th>
			</tr>

			<c:forEach var="reportData" items="${reportList}">
			<tr>
				<td>${reportData.loginId}</td>
				<td>${reportData.reportContent}</td>
				<td>${reportData.reportContentReason}</td>
				<td>${reportData.reportedLoginId}</td>
			</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>