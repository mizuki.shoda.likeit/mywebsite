<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>お気に入りに登録したポケモン確認画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/favoritePokemonConfirm.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
		<h1 align="center"><font color="White">⚡お気に入りに登録したポケモンを確認してみよう⚡</font></h1>
		<br>
		<br>
		<div class="favortePokemonConfirm-area">
			<div class="row">
			<c:forEach var="favoriteData" items="${favoriteDataList}">
				<div class="col-md-4">
					<div class="card mb-4 shadow-sm">
	  					<a href="PokemonTradeDetailFromFavoriteServlet?id=${favoriteData.tradeId}"><img src="picture/${favoriteData.image}" class="card-img-top" alt="${favoriteData.tradePokemonName}"></a>
	  						<div class="card-body">
	    						<h5 class="card-title">${favoriteData.tradePokemonName}</h5>
	    						<p class="card-text">
									タイプ：${favoriteData.firstTypeName}     ${favoriteData.secondTypeName}<br>
									欲しいポケモン：${favoriteData.wantPokemonName}<br>
									ひとこと：${favoriteData.text}<br>
	    						</p>
	  						</div>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
</body>
</html>