<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録したポケモン確認画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/addedPokemonConfirm.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="UserDataServlet"><b><font color="Blue">ユーザー画面に戻る</font></b></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
	</div>
		<h1 align="center"><font color="White">⚡登録したポケモンを確認してみよう⚡</font></h1>
		<br>
		<br>

		<div class="pokemonConfirm-area">
			<div class="row">
			<c:forEach var="tradeData" items="${tradeDataList}">
				<div class="col-md-4">
					<div class="card mb-4 shadow-sm">
	  					<a href="PokemonTradeDetailFromUserDataServlet?id=${tradeData.id}"><img src="picture/${tradeData.image}" class="card-img-top" alt="${tradeData.tradePokemonName}"></a>

	  						<div class="card-body">
	    						<h5 class="card-title">${tradeData.tradePokemonName}</h5>
	    						<p class="card-text">
									タイプ：${tradeData.firstTypeName}    ${tradeData.secondTypeName} <br>
									欲しいポケモン：${tradeData.wantPokemonName}<br>
									ひとこと：${tradeData.text}<br>
	    						</p>
	  						</div>
					</div>
				</div>
			</c:forEach>
			</div>
		</div>
</body>
</html>