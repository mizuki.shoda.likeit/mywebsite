<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理画面</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">

<link href="css/userData.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<div class="user-area">こんにちは！${userLoginInfo.userName}さん
		<a href="UserLogoutServlet"><font color="Red">ログアウト</font></a>
		<br>
		<a href="PokemonSearchServlet"><b><font color="Blue">ポケモン検索画面に戻る</font></b></a>
		<br>
		<c:if test="${userLoginInfo.loginId=='admin'}">
			<a href="UserListServlet"><font color="Blue">ユーザー一覧画面へ</font></a>
		</c:if>
		<br>
		<c:if test="${userLoginInfo.loginId=='admin'}">
			<a href="ReportInfoServlet"><font color="Blue">通報情報一覧画面へ</font></a>
		</c:if>
	</div>
	<br>

	<h1 align="center"><span style="background-color:White">ユーザー管理画面</span></h1>
	<br>

	<div class="userData-area">
		<div class="card">
		<h5 class="card-header">ユーザー情報</h5>

			<div class="card-body">
				<div class="row">
					<div class="col-sm-6">
						<b>ログインID</b>
					</div>

					<div class="col-sm-6">
						${userInfo.loginId}
					</div>
				</div>

			<div class="row">
				<div class="col-sm-6">
					<b>ユーザー名</b>
				</div>
				<div class="col-sm-6">
					${userInfo.userName}
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<b>生年月日</b>
				</div>

				<div class="col-sm-6">
					${userInfo.fmtBirthDate}
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<b>登録日時</b>
				</div>
				<div class="col-sm-6">
					${userInfo.fmtCreateDate}
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<b>更新日時</b>
				</div>
				<div class="col-sm-6">
					${userInfo.fmtUpdateDate}
				</div>
			</div>
			<br>
				<div class="center">
					<a href="UserDataUpdateServlet">
						<input type="submit" class="btn btn-success" value="ユーザー情報を更新する">
					</a>
				</div>
			</div>
		</div>
		<br>
		<div class="card">
			<h5 class="card-header">交換情報を追加する</h5>
				<div class="card-body">
					<div class="center">
						<a href="UserTradeDataUpdateServlet">
							<input type="submit" class="btn btn-success" value="追加画面へ">
						</a>
					</div>
				</div>
		</div>
		<br>
		<div class="card">
			 <div class="card-header">
				<h5>交換情報を確認する</h5>
			 </div>
			 	<br>
			 	<div class="center">
			 		<a href="AddedPokemonConfirmServlet">
			 			<input type="submit" class="btn btn-info" value="確認画面へ">
			 		</a>
			 	</div>
			 	<br>
		</div>
		<br>
		<div class="card">
			 <div class="card-header">
				<h5>お気に入りに追加したポケモンを確認する</h5>
			 </div>
			 	<br>
			 	<div class="center">
			 		<a href="FavoritePokemonConfirmServlet">
			 			<input type="submit" class="btn btn-warning" value="確認画面へ">
			 		</a>
			 	</div>
			 	<br>
		</div>
		<br>
		<div class="card">
			 <div class="card-header">
				<h5>アカウントを削除する</h5>
			 </div>
			 	<br>
			 	<div class="center">
			 		<a href="UserDeleteServlet?id=${userInfo.id}">
			 			<input type="submit" class="btn btn-danger" value="アカウント削除">
			 		</a>
			 	</div>
			 	<br>
		</div>
		<br>
	</div>
</body>
</html>