package beans;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UserDataBeans implements Serializable {
	private int id;
	private String loginId;
	private String password;
	private String userName;
	private String birthDate;
	private String createDate;
	private String updateDate;

	public UserDataBeans() {
	}

	public UserDataBeans(int id, String loginId, String userName) {
		this.id = id;
		this.loginId = loginId;
		this.userName = userName;
	}

	public UserDataBeans(int id, String loginId, String userName, String birthDate, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.userName = userName;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public UserDataBeans(int id, String loginId, String password, String userName, String birthDate, String createDate,
			String updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.password = password;
		this.userName = userName;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getFmtBirthDate() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date formatDate = sdf.parse(this.birthDate);
			String str = new SimpleDateFormat("yyyy年MM月dd日").format(formatDate);
			return str;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getCreateDate() {
		return createDate;
	}

	public String getFmtCreateDate() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date formatDate = sdf.parse(this.createDate);
			String str = new SimpleDateFormat("yyyy年MM月dd日H時mm分").format(formatDate);
			return str;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public String getFmtUpdateDate() {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date formatDate = sdf.parse(this.updateDate);
			String str = new SimpleDateFormat("yyyy年MM月dd日H時mm分").format(formatDate);
			return str;
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
