package beans;

public class ReportDataBeans {
	private int id;
	private int concreatReportId;
	private int userId;
	private int reportUserId;
	private String reportContentReason;

	private String reportContent;
	private String loginId;
	private String reportedLoginId;

	public ReportDataBeans() {
	}

	public ReportDataBeans(String loginId, String reportedLoginId, String reportContent, String reportContentReason) {
		this.loginId=loginId;
		this.reportedLoginId=reportedLoginId;
		this.reportContent=reportContent;
		this.reportContentReason=reportContentReason;
	}



	public ReportDataBeans(int id, int concreatReportId, int userId, int reportUserId, String reportContentReason, String reportContent) {
		this.id=id;
		this.concreatReportId=concreatReportId;
		this.userId=userId;
		this.reportUserId=reportUserId;
		this.reportContentReason=reportContentReason;
		this.reportContent=reportContent;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConcreatReportId() {
		return concreatReportId;
	}

	public void setConcreatReportId(int concreatReportId) {
		this.concreatReportId = concreatReportId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getReportUserId() {
		return reportUserId;
	}

	public void setReportUserId(int reportUserId) {
		this.reportUserId = reportUserId;
	}

	public String getReportContentReason() {
		return reportContentReason;
	}

	public void setReportContentReason(String reportContentReason) {
		this.reportContentReason = reportContentReason;
	}

	public String getReportContent() {
		return reportContent;
	}

	public void setReportContent(String reportContent) {
		this.reportContent = reportContent;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getReportedLoginId() {
		return reportedLoginId;
	}

	public void setReportedLoginId(String reportedLoginId) {
		this.reportedLoginId = reportedLoginId;
	}

}
