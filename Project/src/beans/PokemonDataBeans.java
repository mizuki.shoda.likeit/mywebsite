package beans;

public class PokemonDataBeans {
	private int id;
	private String pokemonName;
	private int firstTypeId;
	private int secondTypeId;
	private String image;

	private String tradePokemonName;
	private String wantPokemonName;
	private String firstTypeName;
	private String secondTypeName;
	private String text;
	private int tradeId;

	public PokemonDataBeans(){
	}

	public PokemonDataBeans(String tradePokemonName, String wantPokemonName, String firstTypeName, String secondTypeName, String image, String text, int tradeId){
		this.tradePokemonName=tradePokemonName;
		this.wantPokemonName=wantPokemonName;
		this.firstTypeName=firstTypeName;
		this.secondTypeName=secondTypeName;
		this.image=image;
		this.text=text;
		this.setTradeId(tradeId);
	}


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPokemonName() {
		return pokemonName;
	}
	public void setPokemonName(String pokemonName) {
		this.pokemonName = pokemonName;
	}
	public int getFirstTypeId() {
		return firstTypeId;
	}
	public void setFirstTypeId(int firstTypeId) {
		this.firstTypeId = firstTypeId;
	}
	public int getSecondTypeId() {
		return secondTypeId;
	}
	public void setSecondTypeId(int secondTypeId) {
		this.secondTypeId = secondTypeId;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTradePokemonName() {
		return tradePokemonName;
	}

	public void setTradePokemonName(String tradePokemonName) {
		this.tradePokemonName = tradePokemonName;
	}

	public String getWantPokemonName() {
		return wantPokemonName;
	}

	public void setWantPokemonName(String wantPokemonName) {
		this.wantPokemonName = wantPokemonName;
	}

	public String getFirstTypeName() {
		return firstTypeName;
	}

	public void setFirstTypeName(String firstTypeName) {
		this.firstTypeName = firstTypeName;
	}

	public String getSecondTypeName() {
		return secondTypeName;
	}

	public void setSecondTypeName(String secondTypeName) {
		this.secondTypeName = secondTypeName;
	}

	public int getTradeId() {
		return tradeId;
	}

	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}

}
