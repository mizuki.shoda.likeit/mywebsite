package beans;

public class MessageDataBeans {
	private int id;
	private String message;
	private int userId;
	private String postDate;
	private int tradeId;

	private String userName;

	public MessageDataBeans(){
	}

	public MessageDataBeans( String message, String postDate, String userName){
		this.message=message;
		this.postDate=postDate;
		this.userName=userName;

	}

	public MessageDataBeans(int id, String message, int userId, String postDate, int tradeId){
		this.id=id;
		this.message=message;
		this.userId=userId;
		this.postDate=postDate;
		this.tradeId=tradeId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getPostDate() {
		return postDate;
	}
	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}
	public int getTradeId() {
		return tradeId;
	}
	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
