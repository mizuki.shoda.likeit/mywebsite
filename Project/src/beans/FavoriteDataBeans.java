package beans;

public class FavoriteDataBeans {
	private int id;
	private int userId;
	private int tradeId;

	private String tradePokemonName;
	private String wantPokemonName;
	private String firstTypeName;
	private String secondTypeName;
	private String image;
	private String text;

	public FavoriteDataBeans() {

	}

	public FavoriteDataBeans(int userId) {
		this.userId=userId;
	}

	public FavoriteDataBeans(int tradeId, String tradePokemonName, String wantPokemonName, String firstTypeName, String secondTypeName, String image, String text) {
		this.tradeId=tradeId;
		this.tradePokemonName=tradePokemonName;
		this.wantPokemonName=wantPokemonName;
		this.firstTypeName=firstTypeName;
		this.secondTypeName=secondTypeName;
		this.image=image;
		this.text=text;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTradeId() {
		return tradeId;
	}
	public void setTradeId(int tradeId) {
		this.tradeId = tradeId;
	}


	public String getTradePokemonName() {
		return tradePokemonName;
	}


	public void setTradePokemonName(String tradePokemonName) {
		this.tradePokemonName = tradePokemonName;
	}


	public String getWantPokemonName() {
		return wantPokemonName;
	}


	public void setWantPokemonName(String wantPokemonName) {
		this.wantPokemonName = wantPokemonName;
	}


	public String getFirstTypeName() {
		return firstTypeName;
	}


	public void setFirstTypeName(String firstTypeName) {
		this.firstTypeName = firstTypeName;
	}


	public String getSecondTypeName() {
		return secondTypeName;
	}


	public void setSecondTypeName(String secondTypeName) {
		this.secondTypeName = secondTypeName;
	}


	public String getImage() {
		return image;
	}


	public void setImage(String image) {
		this.image = image;
	}


	public String getText() {
		return text;
	}


	public void setText(String text) {
		this.text = text;
	}
}
