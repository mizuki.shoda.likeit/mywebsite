package beans;

public class TradeDataBeans {
	private int id;
	private int userId;
	private int tradePokemonId;
	private int wantPokemonId;
	private String text;
	private int finishFlg;

	private int firstTypeId;
	private int secondTypeId;
	private String tradePokemonName;
	private String wantPokemonName;
	private String firstTypeName;
	private String secondTypeName;
	private String image;
	private String pokemonName;

	public TradeDataBeans() {
	}

	public TradeDataBeans(int userId) {
		this.userId=userId;
	}

	public TradeDataBeans(String pokemonName,String text) {
		this.pokemonName=pokemonName;
		this.text=text;
	}

	public TradeDataBeans(int id, String tradePokemonName, String wantPokemonName, String firstTypeName, String secondTypeName, String image, String text) {
		this.id=id;
		this.tradePokemonName=tradePokemonName;
		this.wantPokemonName=wantPokemonName;
		this.firstTypeName=firstTypeName;
		this.secondTypeName=secondTypeName;
		this.image=image;
		this.text=text;
	}

	public TradeDataBeans(int id, int tradePokemonId, String text, String pokemonName, int firstTypeId, int secondTypeId, String image) {
		this.id=id;
		this.tradePokemonId=tradePokemonId;
		this.text=text;
		this.pokemonName=pokemonName;
		this.firstTypeId=firstTypeId;
		this.secondTypeId=secondTypeId;
		this.image=image;
	}

	public TradeDataBeans(int tradePokemonId, int wantPokemonId, String text, int finishFlg) {
		this.tradePokemonId=tradePokemonId;
		this.wantPokemonId=wantPokemonId;
		this.text=text;
		this.finishFlg=finishFlg;
	}

	public TradeDataBeans(int id, int userId, int tradePokemonId, int wantPokemonId, String text, int finishFlg) {
		this.id=id;
		this.userId=userId;
		this.tradePokemonId=tradePokemonId;
		this.wantPokemonId=wantPokemonId;
		this.text=text;
		this.finishFlg=finishFlg;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getTradePokemonId() {
		return tradePokemonId;
	}
	public void setTradePokemonId(int tradePokemonId) {
		this.tradePokemonId = tradePokemonId;
	}
	public int getWantPokemonId() {
		return wantPokemonId;
	}
	public void setWantPokemonId(int wantPokemonId) {
		this.wantPokemonId = wantPokemonId;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getFinishFlg() {
		return finishFlg;
	}
	public void setFinishFlg(int finishFlg) {
		this.finishFlg = finishFlg;
	}

	public String getPokemonName() {
		return pokemonName;
	}

	public void setPokemonName(String pokemonName) {
		this.pokemonName = pokemonName;
	}

	public int getFirstTypeId() {
		return firstTypeId;
	}

	public void setFirstTypeId(int firstTypeId) {
		this.firstTypeId = firstTypeId;
	}

	public int getSecondTypeId() {
		return secondTypeId;
	}

	public void setSecondTypeId(int secondTypeId) {
		this.secondTypeId = secondTypeId;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getTradePokemonName() {
		return tradePokemonName;
	}

	public void setTradePokemonName(String tradePokemonName) {
		this.tradePokemonName = tradePokemonName;
	}

	public String getWantPokemonName() {
		return wantPokemonName;
	}

	public void setWantPokemonName(String wantPokemonName) {
		this.wantPokemonName = wantPokemonName;
	}

	public String getFirstTypeName() {
		return firstTypeName;
	}

	public void setFirstTypeName(String firstTypeName) {
		this.firstTypeName = firstTypeName;
	}

	public String getSecondTypeName() {
		return secondTypeName;
	}

	public void setSecondTypeName(String secondTypeName) {
		this.secondTypeName = secondTypeName;
	}
}
