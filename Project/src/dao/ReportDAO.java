package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.ReportDataBeans;

public class ReportDAO {
	public void addReport(int reportId, int userId, int reportUserId, String reportContentReason)  {
	    Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO report (concrete_report_id,user_id,report_user_id,report_content_reason)VALUES(?,?,?,?)";
        	PreparedStatement pStmt=conn.prepareStatement(sql);

        	pStmt.setInt(1, reportId);
        	pStmt.setInt(2,userId);
        	pStmt.setInt(3,reportUserId);
        	pStmt.setString(4,reportContentReason);

        	pStmt.executeUpdate();

        } catch (SQLException e) {
        	e.printStackTrace();

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();

            	}
        	}
        }
	}

	public void addReportOtherThanReportContentReason(int reportId, int userId, int reportUserId)  {
	    Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO report (concrete_report_id,user_id,report_user_id)VALUES(?,?,?)";
        	PreparedStatement pStmt=conn.prepareStatement(sql);

        	pStmt.setInt(1, reportId);
        	pStmt.setInt(2,userId);
        	pStmt.setInt(3,reportUserId);

        	pStmt.executeUpdate();

        } catch (SQLException e) {
        	e.printStackTrace();

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();

            	}
        	}
        }
	}

	public List<ReportDataBeans> findAllReportData ()  {
	    Connection conn = null;
		List<ReportDataBeans> reportList =new ArrayList<ReportDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM report\r\n" +
        			"INNER JOIN concrete_report ON report.concrete_report_id=concrete_report.id\r\n" +
        			"INNER JOIN user AS u1 ON report.user_id=u1.id\r\n" +
        			"INNER JOIN user AS u2 ON report.report_user_id=u2.id";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	ResultSet rs=pStmt.executeQuery();

        	while (rs.next()) {
        		String loginId =rs.getString("u1.login_id");
        		String reportedloginId=rs.getString("u2.login_id");
        		String reportContent=rs.getString("report_content");
        		String reportContentReason=rs.getString("report_content_reason");

        		ReportDataBeans reportData=new ReportDataBeans(loginId, reportedloginId,reportContent, reportContentReason);
        		reportList.add(reportData);
        	}

        	return reportList;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;

            	}
        	}
        }
	}

	public List<ReportDataBeans> findReportData (String concreteReportId, String loginId, String reportedLoginId)  {
	    Connection conn = null;
		List<ReportDataBeans> reportList =new ArrayList<ReportDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM report\r\n" +
        			"INNER JOIN concrete_report ON report.concrete_report_id=concrete_report.id\r\n" +
        			"INNER JOIN user AS u1 ON report.user_id=u1.id\r\n" +
        			"INNER JOIN user AS u2 ON report.report_user_id=u2.id\r\n" +
        			"WHERE report.id NOT IN(0)";

			if(!concreteReportId.equals("")) {
				sql += " AND report.concrete_report_id ='" + concreteReportId + "'";
			}

			if(!loginId.equals("")) {
				sql += " AND u1.login_id ='" + loginId + "'";
			}

			if(!reportedLoginId.equals("")) {
				sql += " AND u2.login_id ='" + reportedLoginId + "'";
			}

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	ResultSet rs=pStmt.executeQuery();

        	while (rs.next()) {
        		String loginIdData =rs.getString("u1.login_id");
        		String reportedloginIdData=rs.getString("u2.login_id");
        		String reportContentData=rs.getString("report_content");
        		String reportContentReasonData=rs.getString("report_content_reason");

        		ReportDataBeans reportData=new ReportDataBeans(loginIdData, reportedloginIdData,reportContentData, reportContentReasonData);
        		reportList.add(reportData);
        	}

        	return reportList;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;

            	}
        	}
        }
	}

}
