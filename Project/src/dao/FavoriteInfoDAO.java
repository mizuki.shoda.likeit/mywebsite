package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.FavoriteDataBeans;

public class FavoriteInfoDAO {
	public FavoriteDataBeans findByUserIdByTradeId (int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM favorite WHERE trade_id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);
        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int userId=rs.getInt("user_id");

        	return new FavoriteDataBeans(userId);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public FavoriteDataBeans findByUserIdByTradeIdAndUserId (int tradeId, int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM favorite WHERE trade_id=? AND user_id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);
        	pStmt.setInt(2, userId);
        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int userIdData=rs.getInt("user_id");

        	return new FavoriteDataBeans(userIdData);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public void addFavoriteInfo (int userId, int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO favorite (user_id,trade_id)VALUES(?,?)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);
        	pStmt.setInt(2, tradeId);
        	pStmt.executeUpdate();

        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public List<FavoriteDataBeans> findByFavoriteInfo (int userId) {
	    Connection conn = null;
	    List<FavoriteDataBeans> favoriteDataList = new ArrayList<FavoriteDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information\r\n" +
        			"INNER JOIN favorite ON trade_information.id=favorite.trade_id\r\n" +
        			"INNER JOIN user ON trade_information.user_id=user.id\r\n" +
        			"INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id \r\n" +
        			"INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id\r\n" +
        			"LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id \r\n" +
        			"LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id\r\n" +
        			"LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id\r\n" +
        			"LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id\r\n" +
        			"WHERE favorite.user_id=?\r\n" +
        			"AND trade_information.finish_flag NOT IN(1)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	ResultSet rs=pStmt.executeQuery();

			while (rs.next()) {

				int tradeIdData=rs.getInt("trade_information.id");
				String tradePokemonNameData = rs.getString("p1.pokemon_name");
				String wantPokemonNameData=rs.getString("p2.pokemon_name");
				String firstTypeNameData=rs.getString("t1.type_name");
				String secondTypeNameData=rs.getString("t2.type_name");
				String imageData = rs.getString("image");
				String textData=rs.getString("text");



				FavoriteDataBeans favoriteData = new FavoriteDataBeans(tradeIdData,tradePokemonNameData,wantPokemonNameData,firstTypeNameData,secondTypeNameData,imageData,textData);
				favoriteDataList.add(favoriteData);
			}
			return favoriteDataList;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public void deleteFavoriteDataByTradeId(int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="DELETE FROM favorite WHERE trade_id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public void deleteFavoriteDataByUserId(int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="DELETE FROM favorite WHERE user_id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}


}
