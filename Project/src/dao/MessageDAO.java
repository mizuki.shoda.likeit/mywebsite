package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.MessageDataBeans;

public class MessageDAO {
	public List<MessageDataBeans> findByMessage(int tradeId)  {
	    Connection conn = null;
	    List<MessageDataBeans> messageDataList = new ArrayList<MessageDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM message\r\n" +
        			"INNER JOIN trade_information ON message.trade_id=trade_information.id\r\n" +
        			"INNER JOIN user ON message.user_id=user.id\r\n" +
        			"WHERE trade_information.id=?\r\n" +
        			"ORDER BY post_date DESC";
        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	ResultSet rs=pStmt.executeQuery();

			while (rs.next()) {

				String message = rs.getString("message");
				String postDate = rs.getString("post_date");
				String userName= rs.getString("user_name");


				MessageDataBeans messageData = new MessageDataBeans(message, postDate,userName);
				messageDataList.add(messageData);
			}
			return messageDataList;


        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public void addMessage(String message, int userId, int tradeId)  {
	    Connection conn = null;

        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO message (message,user_id,post_date,trade_id)VALUES(?,?,now(),?)";
        	PreparedStatement pStmt=conn.prepareStatement(sql);

        	pStmt.setString(1, message);
        	pStmt.setInt(2,userId);
        	pStmt.setInt(3,tradeId);

        	pStmt.executeUpdate();

        } catch (SQLException e) {
        	e.printStackTrace();

        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();

            	}
        	}
        }
	}

	public void DeleteUserMessageData(int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="DELETE FROM message WHERE user_id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}





}
