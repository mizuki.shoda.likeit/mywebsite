package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;
import beans.PokemonTypeBeans;

public class PokemonTypeDAO {
	public PokemonTypeBeans findByPokemonType (int typeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM type WHERE id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, typeId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int typeIdData = rs.getInt("id");
        	String typeNameData = rs.getString("type_name");


        	return new PokemonTypeBeans(typeIdData,typeNameData);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}
}
