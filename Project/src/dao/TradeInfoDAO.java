package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.TradeDataBeans;

public class TradeInfoDAO {
	public TradeDataBeans findByTradeInfoId (int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information WHERE user_id=? AND finish_flag NOT IN(1)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int tradeInfoId=rs.getInt("id");

        	return new TradeDataBeans(tradeInfoId);
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public TradeDataBeans findByUserIdByTradeId (int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information WHERE id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int userId=rs.getInt("user_id");
        	return new TradeDataBeans(userId);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public List<TradeDataBeans> findByTradeInfo (int userId) {
	    Connection conn = null;
	    List<TradeDataBeans> tradeDataList = new ArrayList<TradeDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information \r\n" +
        			"INNER JOIN user u ON trade_information.user_id=u.id \r\n" +
        			"INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id \r\n" +
        			"INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id\r\n" +
        			"LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id \r\n" +
        			"LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id\r\n" +
        			"LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id\r\n" +
        			"LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id\r\n" +
        			"WHERE trade_information.finish_flag NOT IN(1) \r\n" +
        			"AND  u.id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	ResultSet rs=pStmt.executeQuery();

			while (rs.next()) {

				int tradeIdData=rs.getInt("trade_information.id");
				String tradePokemonNameData = rs.getString("p1.pokemon_name");
				String wantPokemonNameData=rs.getString("p2.pokemon_name");
				String firstTypeNameData=rs.getString("t1.type_name");
				String secondTypeNameData=rs.getString("t2.type_name");
				String imageData = rs.getString("image");
				String textData=rs.getString("text");

				TradeDataBeans tradeData = new TradeDataBeans(tradeIdData,tradePokemonNameData,wantPokemonNameData,firstTypeNameData,secondTypeNameData,imageData,textData);
				tradeDataList.add(tradeData);
			}
			return tradeDataList;

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}


	public TradeDataBeans findByTradePokemonInfoByTradeId (int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information INNER JOIN pokemon ON trade_information.trade_pokemon_id=pokemon.id \r\n" +
        			"WHERE trade_information.id=? \r\n" +
        			"AND trade_information.finish_flag NOT IN (1)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}
        	int tradeIdData=rs.getInt("trade_information.id");
        	int tradePokemonIdData=rs.getInt("trade_pokemon_id");
        	String textData = rs.getString("text");
        	String tradePokemonNameData = rs.getString("pokemon_name");
        	int firstTypeIdData=rs.getInt("first_type_id");
        	int secondTypeIdData=rs.getInt("second_type_id");
        	String imageData=rs.getString("image");

        	return new TradeDataBeans(tradeIdData,tradePokemonIdData,textData,tradePokemonNameData,firstTypeIdData,secondTypeIdData,imageData);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public TradeDataBeans findByWantPokemonInfoByTradeId (int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information \r\n" +
        			"INNER JOIN pokemon ON trade_information.want_pokemon_id=pokemon.id \r\n" +
        			"WHERE trade_information.id=? \r\n" +
        			"AND trade_information.finish_flag NOT IN (1)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int tradeIdData=rs.getInt("trade_information.id");
        	int tradePokemonIdData=rs.getInt("trade_pokemon_id");
        	String textData = rs.getString("text");
        	String tradePokemonNameData = rs.getString("pokemon_name");
        	int firstTypeIdData=rs.getInt("first_type_id");
        	int secondTypeIdData=rs.getInt("second_type_id");
        	String imageData=rs.getString("image");

        	return new TradeDataBeans(tradeIdData,tradePokemonIdData,textData,tradePokemonNameData,firstTypeIdData,secondTypeIdData,imageData);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}
	public void addTradeInfo (int userId, String tradePokemonId, String wantPokemonId, String text) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO trade_information(user_id,trade_pokemon_id,want_pokemon_id,text,finish_flag) VALUES (?,?,?,?,0)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);
        	pStmt.setString(2, tradePokemonId);
        	pStmt.setString(3, wantPokemonId);
        	pStmt.setString(4, text);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public TradeDataBeans findTradeDataOtherThanFinishTrade (int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM trade_information WHERE user_id=? AND finish_flag NOT IN (1) ";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int tradePokemonIdData = rs.getInt("trade_pokemon_id");
        	int wantPokemonIdData = rs.getInt("want_pokemon_id");
        	String textData = rs.getString("text");
        	int finishFlagData=rs.getInt("finish_flag");


        	return new TradeDataBeans(tradePokemonIdData,wantPokemonIdData,textData,finishFlagData);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}


	public void finishPokemonTrade(int tradeId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="UPDATE trade_information SET finish_flag=1 WHERE id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, tradeId);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public void DeleteUserTradeData(int userId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="DELETE FROM trade_information WHERE user_id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, userId);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

}
