package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.PokemonDataBeans;

public class PokemonDAO {
	public List<PokemonDataBeans> findByTradePokemon (String pokemonNameId, String pokemonTypeId, int userId) {
	    Connection conn = null;
	    List<PokemonDataBeans> tradePokemonList = new ArrayList<PokemonDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql= "SELECT * FROM trade_information \r\n" +
        			"INNER JOIN user u ON trade_information.user_id=u.id \r\n" +
        			"INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id \r\n" +
        			"INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id\r\n" +
        			"LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id \r\n" +
        			"LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id\r\n" +
        			"LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id\r\n" +
        			"LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id\r\n" +
        			"WHERE trade_information.finish_flag NOT IN(1)";

        	sql += " AND  u.id NOT IN ( " + userId + ")";


        	if(!pokemonNameId.equals("")) {
				sql += " AND trade_information.trade_pokemon_id= '" + pokemonNameId + "'";
			}

			if(!pokemonTypeId.equals("")) {
				sql += " AND p1.first_type_id ='" + pokemonTypeId + "' "+"OR p1.second_type_id ='" + pokemonTypeId + "'";
			}

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				String tradePokemonNameData = rs.getString("p1.pokemon_name");
				String wantPokemonNameData=rs.getString("p2.pokemon_name");
				String firstTypeNameData=rs.getString("t1.type_name");
				String secondTypeNameData=rs.getString("t2.type_name");
				String imageData = rs.getString("image");
				String textData=rs.getString("text");
				int tradeIdData=rs.getInt("trade_information.id");

				PokemonDataBeans tradePokemonData = new PokemonDataBeans(tradePokemonNameData, wantPokemonNameData,firstTypeNameData, secondTypeNameData, imageData, textData,tradeIdData);
				tradePokemonList.add(tradePokemonData);
			}

			return tradePokemonList;
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public List<PokemonDataBeans> findByTradePokemonByWantPokemon (String pokemonNameId, String pokemonTypeId, int userId) {
	    Connection conn = null;
	    List<PokemonDataBeans> tradePokemonList = new ArrayList<PokemonDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql= "SELECT * FROM trade_information \r\n" +
        			"INNER JOIN user u ON trade_information.user_id=u.id \r\n" +
        			"INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id \r\n" +
        			"INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id\r\n" +
        			"LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id \r\n" +
        			"LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id\r\n" +
        			"LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id\r\n" +
        			"LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id\r\n" +
        			"WHERE trade_information.finish_flag NOT IN(1)";

        	sql += " AND  u.id NOT IN ( " + userId + ")";

			if(!pokemonNameId.equals("")) {
				sql += " AND trade_information.want_pokemon_id= '" + pokemonNameId + "'";
			}

			if(!pokemonTypeId.equals("")) {
				sql += " AND p2.first_type_id ='" + pokemonTypeId + "' "+"OR p2.second_type_id ='" + pokemonTypeId + "'";
			}

			PreparedStatement pStmt = conn.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();

			while (rs.next()) {
				String tradePokemonNameData = rs.getString("p1.pokemon_name");
				String wantPokemonNameData=rs.getString("p2.pokemon_name");
				String firstTypeNameData=rs.getString("t1.type_name");
				String secondTypeNameData=rs.getString("t2.type_name");
				String imageData = rs.getString("image");
				String textData=rs.getString("text");
				int tradeIdData=rs.getInt("trade_information.id");

				PokemonDataBeans tradePokemonData = new PokemonDataBeans(tradePokemonNameData, wantPokemonNameData,firstTypeNameData, secondTypeNameData, imageData, textData,tradeIdData);
				tradePokemonList.add(tradePokemonData);
			}

			return tradePokemonList;
        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}





}
