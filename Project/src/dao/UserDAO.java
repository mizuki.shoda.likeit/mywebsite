package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.UserDataBeans;

public class UserDAO {
	public UserDataBeans findByLoginInfo (String loginId, String password) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM user WHERE login_id=? and password=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);

			Encode encode = new Encode();
			String encodePass = encode.EncodePass(password);

        	pStmt.setString(1, loginId);
        	pStmt.setString(2, encodePass);
        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int id=rs.getInt("id");
        	String loginIdData = rs.getString("login_id");
        	String userNameData = rs.getString("user_name");
        	return new UserDataBeans(id,loginIdData, userNameData);


        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public List<UserDataBeans> findAll()  {
	    Connection conn = null;
	    List<UserDataBeans> userList = new ArrayList<UserDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM user WHERE id NOT IN(1)";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	ResultSet rs=pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String userName = rs.getString("user_name");
				String birthDate = rs.getString("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				UserDataBeans userData = new UserDataBeans(id, loginId, password,userName, birthDate, createDate, updateDate);
				userList.add(userData);
			}
			return userList;


        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}


	public List<UserDataBeans> findByUserList(String loginId, String userName, String beginningBirthDate, String endBirthDate){
	    Connection conn = null;
	    List<UserDataBeans> userList = new ArrayList<UserDataBeans>();
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM user WHERE id NOT IN(1)";

			if(!loginId.equals("")) {
				sql += " AND login_id ='" + loginId + "'";

			}

			if(!userName.equals("")) {
				sql += " AND user_name LIKE  " + "'%" + userName + "%'";

			}

			if(!beginningBirthDate.equals("")) {
				sql += " AND birth_date " + ">= '" + beginningBirthDate + "'";

			}

			if(!endBirthDate.equals("")) {
				sql += " AND birth_date " + "<= '" + endBirthDate + "'";
			}

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	ResultSet rs=pStmt.executeQuery();

			while (rs.next()) {
				int id = rs.getInt("id");
				String loginIdData = rs.getString("login_id");
				String password = rs.getString("password");
				String userNameData = rs.getString("user_name");
				String birthDate = rs.getString("birth_date");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");

				UserDataBeans userData = new UserDataBeans(id, loginIdData, password,userNameData, birthDate, createDate, updateDate);
				userList.add(userData);
			}
			return userList;


        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public void addUser(String loginId, String password, String userName, String birthDate){
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="INSERT INTO user(login_id,password,user_name,birth_date,create_date,update_date) VALUES(?,?,?,?,now(),now())";

        	PreparedStatement pStmt = conn.prepareStatement(sql);

			Encode encode = new Encode();
			String encodePass = encode.EncodePass(password);

        	pStmt.setString(1, loginId);
        	pStmt.setString(2, encodePass);
        	pStmt.setString(3, userName);
        	pStmt.setString(4, birthDate);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public UserDataBeans searchUserByLoginId(String loginId) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM user WHERE login_id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setString(1, loginId);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int idData = rs.getInt("id");
        	String loginIdData = rs.getString("login_id");
        	String userNameData = rs.getString("user_name");
        	String birthDate=rs.getString("birth_date");
        	String createDate=rs.getString("create_date");
        	String updateDate=rs.getString("update_date");

        	return new UserDataBeans(idData,loginIdData, userNameData,birthDate,createDate,updateDate);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public UserDataBeans searchUserById(int id) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="SELECT * FROM user WHERE id=?";

        	PreparedStatement pStmt=conn.prepareStatement(sql);
        	pStmt.setInt(1, id);

        	ResultSet rs=pStmt.executeQuery();

        	if (!rs.next()) {
        		return null;
        		}

        	int idData = rs.getInt("id");
        	String loginIdData = rs.getString("login_id");
        	String userNameData = rs.getString("user_name");
        	String birthDate=rs.getString("birth_date");
        	String createDate=rs.getString("create_date");
        	String updateDate=rs.getString("update_date");

        	return new UserDataBeans(idData,loginIdData, userNameData,birthDate,createDate,updateDate);

        } catch (SQLException e) {
        	e.printStackTrace();
        	return null;
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
                		return null;
            	}
        	}
        }
	}

	public void updateUser(String password,String userName,String birthDate, String id) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="UPDATE user SET password=?,user_name=?,birth_date=?,update_date=now() WHERE id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);

			Encode encode = new Encode();
			String encodePass = encode.EncodePass(password);

        	pStmt.setString(1, encodePass);
        	pStmt.setString(2, userName);
        	pStmt.setString(3, birthDate);
        	pStmt.setString(4, id);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}

	public void updateUserOtherThanPassword(String userName,String birthDate, String id) {
		 Connection conn = null;
	        try {
	            // データベースへ接続
	        	conn = DBManager.getConnection();

	        	String sql="UPDATE user SET user_name=?,birth_date=?,update_date=now() WHERE id=?";

	        	PreparedStatement pStmt = conn.prepareStatement(sql);
	        	pStmt.setString(1, userName);
	        	pStmt.setString(2, birthDate);
	        	pStmt.setString(3, id);

	        	pStmt.executeUpdate();

	        } catch (SQLException e) {
	        	e.printStackTrace();
	        } finally {
	        	// データベース切断
	          if (conn != null) {
	          	try {
	                		conn.close();
	            	} catch (SQLException e) {
	                		e.printStackTrace();
	            	}
	        	}
	        }
	}

	public void DeleteUserData(int id) {
	    Connection conn = null;
        try {
            // データベースへ接続
        	conn = DBManager.getConnection();

        	String sql="DELETE FROM user WHERE id=?";

        	PreparedStatement pStmt = conn.prepareStatement(sql);
        	pStmt.setInt(1, id);

        	pStmt.executeUpdate();


        } catch (SQLException e) {
        	e.printStackTrace();
        } finally {
        	// データベース切断
          if (conn != null) {
          	try {
                		conn.close();
            	} catch (SQLException e) {
                		e.printStackTrace();
            	}
        	}
        }
	}


}
