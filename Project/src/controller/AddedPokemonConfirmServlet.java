package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TradeDataBeans;
import beans.UserDataBeans;
import dao.TradeInfoDAO;

/**
 * Servlet implementation class AddedPokemonConfirmServlet
 */
@WebServlet("/AddedPokemonConfirmServlet")
public class AddedPokemonConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddedPokemonConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}
		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//ユーザーIDから交換情報を取得
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		List<TradeDataBeans> tradeDataList = new ArrayList<TradeDataBeans>();
		tradeDataList=tradeInfoDAO.findByTradeInfo(userId);
		request.setAttribute("tradeDataList", tradeDataList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/addedPokemonConfirm.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
