package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserLoginServlet
 */
@WebServlet("/UserLoginServlet")
public class UserLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッション取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログインセッションがあればポケモン検索画面に遷移する
		if(userLoginInfo != null){
			response.sendRedirect("PokemonSearchServlet");
			return;
		}

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメーターからログインIDとパスワードを取得
		request.setCharacterEncoding("UTF-8");

		String loginId=request.getParameter("loginId");
		String password=request.getParameter("password");

		//ログインIDとパスワードが登録されているか確認
		UserDAO userDAO=new UserDAO();
		UserDataBeans userLoginInfo=userDAO.findByLoginInfo(loginId,password);

		//登録情報がなければエラーメッセージを表示
		if(userLoginInfo==null) {
			request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります。");

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//ログイン情報をセッションに保存してポケモン検索画面にリダイレクト
		HttpSession session=request.getSession();
		session.setAttribute("userLoginInfo", userLoginInfo);

		response.sendRedirect("PokemonSearchServlet");
	}

}
