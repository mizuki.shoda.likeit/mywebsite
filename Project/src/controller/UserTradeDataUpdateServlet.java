package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TradeDataBeans;
import beans.UserDataBeans;
import dao.TradeInfoDAO;



/**
 * Servlet implementation class UserTradeDataUpdateServlet
 */
@WebServlet("/UserTradeDataUpdateServlet")
public class UserTradeDataUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserTradeDataUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//ユーザーIDをもとに交換完了していない交換情報を取得
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		TradeDataBeans tradeInfo=tradeInfoDAO.findTradeDataOtherThanFinishTrade(userId);

		request.setAttribute("tradeInfo", tradeInfo);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userTradeDataUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメーターから交換情報取得
		request.setCharacterEncoding("UTF-8");
		String tradePokemonId = request.getParameter("tradePokemonId");
		String wantPokemonId = request.getParameter("wantPokemonId");
		String text = request.getParameter("text");

		//ログインセッションからユーザーIDを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
		int userId=userLoginInfo.getId();

		//交換情報を追加する
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		tradeInfoDAO.addTradeInfo(userId, tradePokemonId, wantPokemonId, text);

		response.sendRedirect("AddedPokemonConfirmServlet");
	}
}
