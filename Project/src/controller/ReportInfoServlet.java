package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ReportDataBeans;
import beans.UserDataBeans;
import dao.ReportDAO;

/**
 * Servlet implementation class ReportInfoServlet
 */
@WebServlet("/ReportInfoServlet")
public class ReportInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReportInfoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//通報情報を取得
		List <ReportDataBeans> reportList=new ArrayList<ReportDataBeans>();
		ReportDAO reportDAO=new ReportDAO();
		reportList=reportDAO.findAllReportData();
		request.setAttribute("reportList", reportList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/reportInfo.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//リクエストパラメーターからデータを取得
		request.setCharacterEncoding("UTF-8");
		String concreteReportId=request.getParameter("concreteReportId");
		String loginId=request.getParameter("loginId");
		String reportedLoginId=request.getParameter("reportedLoginId");

		//入力されたデータをもとに通報情報を検索
		List <ReportDataBeans> reportList=new ArrayList<ReportDataBeans>();
		ReportDAO reportDAO=new ReportDAO();
		reportList=reportDAO.findReportData(concreteReportId, loginId, reportedLoginId);
		request.setAttribute("reportList", reportList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/reportInfo.jsp");
		dispatcher.forward(request, response);
	}

}
