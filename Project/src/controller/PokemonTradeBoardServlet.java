package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.MessageDataBeans;
import beans.TradeDataBeans;
import beans.UserDataBeans;
import dao.MessageDAO;
import dao.TradeInfoDAO;
import dao.UserDAO;

/**
 * Servlet implementation class PokemonTradeBoardServlet
 */
@WebServlet("/PokemonTradeBoardServlet")
public class PokemonTradeBoardServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonTradeBoardServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		request.setCharacterEncoding("UTF-8");
		//JSPからトレードIDを取得
		String tradeIdData=request.getParameter("id");
		int tradeId=Integer.parseInt(tradeIdData);
		request.setAttribute("tradeId", tradeId);

		//トレードIDからユーザーIDを入手
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		TradeDataBeans userIdData=tradeInfoDAO.findByUserIdByTradeId(tradeId);
		int tradeUserId=userIdData.getUserId();

		//ログインセッションから自身のユーザーIDを取得
		int userId=userLoginInfo.getId();

		request.setAttribute("userLoginInfo", userLoginInfo);

		//JSPからメッセージパラメーターを取得
		String message=request.getParameter("message");

		//パラメーターがnullでなければメッセージ情報を追加
		if(message != null) {
			MessageDAO messageDAO=new MessageDAO();
			messageDAO.addMessage(message, userId, tradeId);
		}

		//トレードIDでメッセージデータを取得する
		MessageDAO messageDAO=new MessageDAO();
		List<MessageDataBeans> messageDataList=messageDAO.findByMessage(tradeId);
		request.setAttribute("messageDataList", messageDataList);

		//交換相手のユーザーIDからユーザー情報を取得
		UserDAO userDAO=new UserDAO();
		UserDataBeans userInfo=userDAO.searchUserById(tradeUserId);
		request.setAttribute("userInfo", userInfo);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/pokemonTradeBoard.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
