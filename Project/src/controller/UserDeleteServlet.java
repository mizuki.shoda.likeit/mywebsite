package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.FavoriteInfoDAO;
import dao.MessageDAO;
import dao.TradeInfoDAO;
import dao.UserDAO;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//JSPからユーザーIDを取得
		String id=request.getParameter("id");
		int userId=Integer.parseInt(id);

		//ユーザー情報取得
		UserDAO userDAO=new UserDAO();
		UserDataBeans userInfo=userDAO.searchUserById(userId);
		request.setAttribute("userInfo", userInfo);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//JSPからユーザーIDを取得
		String id=request.getParameter("id");
		int userId=Integer.parseInt(id);

		//セッションスコープからログインIDを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
		String loginId=userLoginInfo.getLoginId();

		//ユーザーテーブルからデータを削除
		UserDAO userDAO=new UserDAO();
		userDAO.DeleteUserData(userId);

		//交換情報テーブルからデータを削除
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		tradeInfoDAO.DeleteUserTradeData(userId);

		//メッセージテーブルからデータを削除
		MessageDAO messageDAO=new MessageDAO();
		messageDAO.DeleteUserMessageData(userId);

		//お気に入りテーブルからデータを削除
		FavoriteInfoDAO favoriteInfoDAO=new FavoriteInfoDAO();
		favoriteInfoDAO.deleteFavoriteDataByUserId(userId);

		if(loginId.equals("admin")){
			response.sendRedirect("UserListServlet");
			return;
		}
		session.removeAttribute("userLoginInfo");
		response.sendRedirect("UserLoginServlet");


	}

}
