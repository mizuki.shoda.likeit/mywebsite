package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.FavoriteDataBeans;
import beans.UserDataBeans;
import dao.FavoriteInfoDAO;


/**
 * Servlet implementation class FavoritePokemonConfirmServlet
 */
@WebServlet("/FavoritePokemonConfirmServlet")
public class FavoritePokemonConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoritePokemonConfirmServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}
		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//ユーザーIDをもとにお気に入りに登録したポケモン情報を取得
		FavoriteInfoDAO favoriteInfoDAO=new FavoriteInfoDAO();
		List<FavoriteDataBeans> favoriteDataList=favoriteInfoDAO.findByFavoriteInfo(userId);
		request.setAttribute("favoriteDataList", favoriteDataList);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/favoritePokemonConfirm.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//ヒドゥンパラメーターからトレードIDを取得
		request.setCharacterEncoding("UTF-8");
		String id=request.getParameter("tradeId");
		int tradeId=Integer.parseInt(id);

		//ログインセッションからユーザーIDを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
		int userId=userLoginInfo.getId();

		//お気に入り情報を登録
		FavoriteInfoDAO favoriteInfoDAO=new FavoriteInfoDAO();
		favoriteInfoDAO.addFavoriteInfo(userId, tradeId);

		response.sendRedirect("FavoritePokemonConfirmServlet");
	}

}
