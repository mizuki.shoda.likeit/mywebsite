package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PokemonDataBeans;
import beans.UserDataBeans;
import dao.PokemonDAO;

/**
 * Servlet implementation class TradePokemonSearch
 */
@WebServlet("/TradePokemonSearchServlet")
public class TradePokemonSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TradePokemonSearchServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//交換に出すポケモンを検索
		//交換に出すポケモンの検索情報を取得
		request.setCharacterEncoding("UTF-8");
		String tradePokemonNameId = request.getParameter("tradePokemonNameId");
		String tradePokemonTypeId = request.getParameter("tradePokemonTypeId");

		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//交換に出されているポケモンを検索
		PokemonDAO pokemonDAO=new PokemonDAO();
		List<PokemonDataBeans>tradePokemonData=pokemonDAO.findByTradePokemon(tradePokemonNameId, tradePokemonTypeId,userId);
		request.setAttribute("tradePokemonList", tradePokemonData);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/pokemonSearchResult.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
