package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class AddUserServlet
 */
@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/addUser.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		//リクエストパラメーターからデータを取得
		request.setCharacterEncoding("UTF-8");
		String loginId=request.getParameter("loginId");
		String password=request.getParameter("password");
		String confirmPassword=request.getParameter("confirmPassword");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");

		//パスワードと確認用パスワードが異なっていた場合、エラーメッセージを表示
		if(!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "パスワードと確認用パスワードが異なります。");

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/addUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力されたログインIDが既に使われていないかを検索
		UserDAO userDAO=new UserDAO();
		UserDataBeans userInfo=userDAO.searchUserByLoginId(loginId);

		//既に登録されていた場合、エラーメッセージを表示
		if(userInfo!=null) {
			request.setAttribute("errMsg2", "既に登録されているログインIDです");

			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/addUser.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//入力された情報を登録
		userDAO.addUser(loginId, confirmPassword, userName, birthDate);

		//ログイン情報を検索し、セッションスコープにセット
		UserDataBeans userLoginInfo=userDAO.findByLoginInfo(loginId,password);
		HttpSession session=request.getSession();
		session.setAttribute("userLoginInfo", userLoginInfo);

		response.sendRedirect("PokemonSearchServlet");
	}

}
