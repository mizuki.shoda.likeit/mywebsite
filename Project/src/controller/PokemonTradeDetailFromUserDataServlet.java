package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PokemonTypeBeans;
import beans.TradeDataBeans;
import beans.UserDataBeans;
import dao.PokemonTypeDAO;
import dao.TradeInfoDAO;
import dao.UserDAO;



/**
 * Servlet implementation class PokemonTradeDetailServlet
 */
@WebServlet("/PokemonTradeDetailFromUserDataServlet")
public class PokemonTradeDetailFromUserDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonTradeDetailFromUserDataServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//トレードID取得
		String id = request.getParameter("id");
		int tradeId=Integer.parseInt(id);
		request.setAttribute("tradeId", tradeId);

		//ユーザー情報取得
		UserDAO userDAO=new UserDAO();
		UserDataBeans userInfo=userDAO.searchUserById(userId);
		request.setAttribute("userInfo", userInfo);

		//交換情報取得
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		TradeDataBeans tradePokemonData=tradeInfoDAO.findByTradePokemonInfoByTradeId(tradeId);
		request.setAttribute("tradePokemonData", tradePokemonData);

		//交換情報から交換に出すポケモンのタイプを取得
		int tradeFirstTypeId=tradePokemonData.getFirstTypeId();
		int tradeSecondTypeId=tradePokemonData.getSecondTypeId();

		PokemonTypeDAO pokemonTypeDAO=new PokemonTypeDAO();
		PokemonTypeBeans tradePokemonFirstTypeData=pokemonTypeDAO.findByPokemonType(tradeFirstTypeId);
		request.setAttribute("tradePokemonFirstTypeData", tradePokemonFirstTypeData);

		PokemonTypeBeans tradePokemonSecondTypeData=pokemonTypeDAO.findByPokemonType(tradeSecondTypeId);
		request.setAttribute("tradePokemonSecondTypeData",tradePokemonSecondTypeData);

		//欲しいポケモン情報取得
		TradeDataBeans wantPokemonData=tradeInfoDAO.findByWantPokemonInfoByTradeId(tradeId);
		request.setAttribute("wantPokemonData", wantPokemonData);

		//欲しいポケモン情報から交換に出すポケモンのタイプを取得
		int WantFirstTypeId=wantPokemonData.getFirstTypeId();
		int WantSecondTypeId=wantPokemonData.getSecondTypeId();

		PokemonTypeBeans wantPokemonFirstTypeData=pokemonTypeDAO.findByPokemonType(WantFirstTypeId);
		request.setAttribute("wantPokemonFirstTypeData", wantPokemonFirstTypeData);

		PokemonTypeBeans wantPokemonSecondTypeData=pokemonTypeDAO.findByPokemonType(WantSecondTypeId);
		request.setAttribute("wantPokemonSecondTypeData",wantPokemonSecondTypeData);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/pokemonTradeDetail.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

}
