package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.TradeDataBeans;
import beans.UserDataBeans;
import dao.ReportDAO;
import dao.TradeInfoDAO;

/**
 * Servlet implementation class AddReportServlet
 */
@WebServlet("/AddReportServlet")
public class AddReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddReportServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//リクエストパラメーターから通報情報を取得
		request.setCharacterEncoding("UTF-8");
		String id=request.getParameter("reportId");
		int reportId=Integer.parseInt(id);
		String reportReason=request.getParameter("reportReason");

		//ヒドゥンパラメーターからトレードIDを取得
		String hiddenId=request.getParameter("tradeId");
		int tradeId=Integer.parseInt(hiddenId);

		//トレードIDから交換相手のユーザーID取得
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		TradeDataBeans userIdData=tradeInfoDAO.findByUserIdByTradeId(tradeId);
		int tradeUserId=userIdData.getUserId();

		//ログインセッションからユーザーIDを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
		int userId=userLoginInfo.getId();

		//TODO理由が未記入だった場合、それ以外を追加
		ReportDAO reportDAO=new ReportDAO();
		if(reportReason == null) {
			reportDAO.addReportOtherThanReportContentReason(reportId, userId, tradeUserId);
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/reportFinish.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//通報テーブルに情報を追加
		reportDAO.addReport(reportId, userId, tradeUserId, reportReason);
		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/reportFinish.jsp");
		dispatcher.forward(request, response);
	}
}
