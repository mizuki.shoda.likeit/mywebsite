package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDAO;

/**
 * Servlet implementation class UserDataUpdateServlet
 */
@WebServlet("/UserDataUpdateServlet")
public class UserDataUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDataUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//ログインセッションからユーザーIDを取得
		int userId=userLoginInfo.getId();

		//ユーザーIDをもとにユーザー情報を取得
		UserDAO userDAO=new UserDAO();
		UserDataBeans userInfo=userDAO.searchUserById(userId);
		request.setAttribute("userInfo", userInfo);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userDataUpdate.jsp");
		dispatcher.forward(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//リクエストパラメーターから情報を取得
		request.setCharacterEncoding("UTF-8");
		String id=request.getParameter("id");
		String password=request.getParameter("password");
		String confirmPassword=request.getParameter("confirmPassword");
		String userName=request.getParameter("userName");
		String birthDate=request.getParameter("birthDate");

		//パスワードと確認用パスワードが異なった際、エラーメッセージを表示
		if(!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "パスワードと確認用パスワードが異なります。");
			RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/userDataUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//パスワードが入力されていなければ、パスワード情報以外を上書き処理
		UserDAO userDAO=new UserDAO();
		if(password.equals("")) {
			userDAO.updateUserOtherThanPassword(userName,birthDate,id);
			//ログインセッションを上書き
			HttpSession session=request.getSession();
			UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
			userLoginInfo.setUserName(userName);
			session.setAttribute("userLoginInfo", userLoginInfo);
			response.sendRedirect("UserDataServlet");
			return;
		}

		//ユーザー情報の上書き処理
		userDAO.updateUser(password,userName,birthDate,id);
		//ログインセッションを上書き
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");
		userLoginInfo.setUserName(userName);
		session.setAttribute("userLoginInfo", userLoginInfo);

		response.sendRedirect("UserDataServlet");
	}
}


