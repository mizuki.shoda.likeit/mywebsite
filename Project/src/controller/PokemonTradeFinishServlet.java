package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.TradeInfoDAO;

/**
 * Servlet implementation class PokemonTradeFinishServlet
 */
@WebServlet("/PokemonTradeFinishServlet")
public class PokemonTradeFinishServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonTradeFinishServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションを取得
		HttpSession session=request.getSession();
		UserDataBeans userLoginInfo=(UserDataBeans)session.getAttribute("userLoginInfo");

		//ログイン情報がなければログイン画面にリダイレクトする
		if(userLoginInfo == null) {
			response.sendRedirect("UserLoginServlet");
			return;
		}

		//トレードID取得
		String id = request.getParameter("id");
		int tradeId=Integer.parseInt(id);
		request.setAttribute("tradeId", tradeId);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/pokemonTradeFinish.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//ログインセッションからユーザーIDを取得
		//JSPからトレードIDを取得
		String tradeIdData=request.getParameter("id");
		int tradeId=Integer.parseInt(tradeIdData);
		request.setAttribute("tradeId", tradeId);

		//ユーザーIDをもとに交換終了処理
		TradeInfoDAO tradeInfoDAO=new TradeInfoDAO();
		tradeInfoDAO.finishPokemonTrade(tradeId);

		response.sendRedirect("AddedPokemonConfirmServlet");

	}

}
