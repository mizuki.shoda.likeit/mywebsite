/* DB、テーブル作成*/
CREATE DATABASE pokemon_db DEFAULT CHARACTER SET utf8;

CREATE TABLE user(
	id SERIAL,
	login_id varchar(255) UNIQUE NOT NULL,
	password varchar(256) NOT NULL,
	user_name varchar(256) NOT NULL,
	birth_date DATE NOT NULL,
	create_date DATETIME NOT NULL,
	update_date DATETIME NOT NULL
);

CREATE TABLE pokemon(
	id SERIAL,
	pokemon_name varchar(255) UNIQUE NOT NULL,
	first_type_id int NOT NULL,
	second_type_id int,
	image varchar(256) NOT NULL
);

CREATE TABLE trade_information(
	id SERIAL,
	user_id int NOT NULL,
	trade_pokemon_id int NOT NULL,
	want_pokemon_id int NOT NULL,
	text varchar(256) NOT NULL,
	finish_flag int NOT NULL
);

CREATE TABLE type(
	id SERIAL,
	type_name varchar(255) UNIQUE NOT NULL
);

CREATE TABLE message(
	id SERIAL,
	message varchar(256),
	user_id int NOT NULL,
	post_date DATETIME NOT NULL,
	trade_id int NOT NULL
);

CREATE TABLE favorite(
	id SERIAL,
	user_id int NOT NULL,
	trade_id int NOT NULL
);

CREATE TABLE report(
	id SERIAL,
	concrete_report_id int NOT NULL,
	user_id int NOT NULL,
	report_user_id int NOT NULL,
	report_content_reason varchar(256)
);


CREATE TABLE concrete_report(
	id SERIAL,
	report_content varchar(256) NOT NULL
);

INSERT INTO concrete_report (report_content) VALUES 
	('誹謗・中傷の書き込み'),
	('個人情報の掲載'),
	('出会い系・猥褻サイトのリンク'),
	('営利目的となる商用利用'),
	('猥褻表現'),
	('犯罪・自殺をほのめかす内容'),
	('スパム'),
	('迷惑行為'),
	('その他');

INSERT INTO type (type_name) VALUES 
	('ノーマル'),
	('ほのお'),
	('みず'),
	('でんき'),
	('くさ'),
	('こおり'),
	('かくとう'),
	('どく'),
	('じめん'),
	('ひこう'),
	('エスパー'),
	('むし'),
	('いわ'),
	('ゴースト'),
	('ドラゴン'),
	('あく'),
	('はがね'),
	('フェアリー');
	
INSERT INTO pokemon (pokemon_name,first_type_id,second_type_id,image) VALUES
	('フシギダネ',5,8,'hushigidane.png'),
	('フシギソウ',5,8,'hushigisou.png'),
	('フシギバナ',5,8,'hushigibana.jpg'),
	('ゼニガメ',3,NULL,'zenigame.jpg'),
	('カメール',3,NULL,'kame-ru.jpg'),
	('カメックス',3,NULL,'kamekkusu.jpg'),
	('ヒトカゲ',2,NULL,'hitokage2.jpg'),
	('リザード',2,NULL,'riza-do.jpg'),
	('リザードン',2,10,'riza.png'),
	('ピカチュウ',4,NULL,'pikatyuu.jpg');
	
INSERT INTO user VALUES
	(1,'admin','password','管理者','1990-03-22',now(),now());
	
SELECT * FROM user WHERE login_id='admin' and password='password';

/* trade_informationを軸にポケモンを検索する（OK）*/
SELECT * FROM trade_information 
INNER JOIN user u ON trade_information.user_id=u.id 
INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id 
INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id
LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id 
LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id
LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id
LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id
WHERE trade_information.finish_flag NOT IN(1) 
AND  u.id NOT IN (18)
AND trade_information.trade_pokemon_id=2 AND p1.first_type_id=5 OR p1.second_type_id=5;

/* trade_informationを軸にポケモンを検索する(欲しいポケモンから)*/
SELECT * FROM trade_information 
INNER JOIN user u ON trade_information.user_id=u.id 
INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id 
INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id
LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id 
LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id
LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id
LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id
WHERE trade_information.finish_flag NOT IN(1) 
AND u.id NOT IN(1)
AND trade_information.want_pokemon_id=9 AND p2.first_type_id=10 OR p2.second_type_id=10;

SELECT * FROM user WHERE id NOT IN(1);


/* 登録したポケモンを確認する*/
SELECT * FROM trade_information 
INNER JOIN user u ON trade_information.user_id=u.id 
INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id 
INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id
LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id 
LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id
LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id
LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id
WHERE trade_information.finish_flag NOT IN(1) 
AND  u.id=1


/* メッセージデータを取得(再作成)*/
SELECT * FROM message
INNER JOIN trade_information ON message.trade_id=trade_information.id
INNER JOIN user ON message.user_id=user.id
WHERE trade_information.id=23
ORDER BY post_date DESC

SELECT * FROM trade_information WHERE user_id=18 AND finish_flag NOT IN(1);

/* メッセージデータを追加*/
INSERT INTO message (message,user_id,post_date,trade_id)VALUES('やったー！',1,now(),2);

UPDATE trade_information SET finish_flag=0 WHERE user_id=2;


SELECT * FROM trade_information WHERE user_id=2 AND finish_flag NOT IN (1);

UPDATE trade_information SET finish_flag=1 WHERE user_id=2;

SELECT * FROM trade_information INNER JOIN pokemon ON trade_information.trade_pokemon_id=pokemon.id WHERE user_id=18 AND trade_information.finish_flag NOT IN (1)

/* お気に入り情報を追加*/
INSERT INTO favorite (user_id,trade_id)VALUES(1,5);

/* お気に入り情報を検索*/
SELECT * FROM trade_information
INNER JOIN favorite ON trade_information.id=favorite.trade_id
INNER JOIN user ON trade_information.user_id=user.id
INNER JOIN pokemon p1 ON trade_information.trade_pokemon_id=p1.id 
INNER JOIN pokemon p2 ON trade_information.want_pokemon_id=p2.id
LEFT OUTER JOIN type t1 ON p1.first_type_id=t1.id 
LEFT OUTER JOIN type t2 ON p1.second_type_id=t2.id
LEFT OUTER JOIN type t3 ON p2.first_type_id=t3.id
LEFT OUTER JOIN type t4 ON p2.second_type_id=t4.id
WHERE favorite.user_id=18
AND trade_information.finish_flag NOT IN(1)

DROP TABLE favorite

SELECT * FROM trade_information 
INNER JOIN pokemon ON trade_information.trade_pokemon_id=pokemon.id 
WHERE user_id=18 
AND trade_information.finish_flag NOT IN (1)

SELECT * FROM trade_information WHERE id=36

SELECT * FROM favorite WHERE trade_id=35

DELETE FROM favorite WHERE trade_id=35

SELECT * FROM trade_information INNER JOIN pokemon ON trade_information.trade_pokemon_id=pokemon.id 
WHERE trade_information.id=38 
AND trade_information.finish_flag NOT IN (1)

SELECT * FROM trade_information 
INNER JOIN pokemon ON trade_information.want_pokemon_id=pokemon.id 
WHERE trade_information.id=38
AND trade_information.finish_flag NOT IN (1)


UPDATE trade_information SET finish_flag=1 WHERE id=?

/* 通報情報登録*/
INSERT INTO report (concrete_report_id,user_id,report_user_id,report_content_reason)VALUES(8,18,19,'違うポケモンを交換に出してきた！');


/* すべての通報情報検索*/
SELECT * FROM report
INNER JOIN concrete_report ON report.concrete_report_id=concrete_report.id
INNER JOIN user AS u1 ON report.user_id=u1.id
INNER JOIN user AS u2 ON report.report_user_id=u2.id

/* 通報情報検索*/
SELECT * FROM report
INNER JOIN concrete_report ON report.concrete_report_id=concrete_report.id
INNER JOIN user AS u1 ON report.user_id=u1.id
INNER JOIN user AS u2 ON report.report_user_id=u2.id
WHERE report.id NOT IN(0)
AND report.concrete_report_id=1
AND u1.login_id='2'
AND u2.login_id='elite'
